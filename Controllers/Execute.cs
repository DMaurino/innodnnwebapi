﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using DotNetNuke.Web.Api;
using System.Net.Http;
using System.Net;
using DotNetNuke.Services.Exceptions;
using NPoco;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using DotNetNuke.Security.Permissions;
using System.Configuration;

namespace WebApi.Controllers
{
    public class ExecuteController : DnnApiController
    {
        private static Database _database;
        private static object _lock = new object();
        private static string[] _whitelist;
        private static HashSet<string> _sprocsWithUserId;

        static ExecuteController()
        {
            _database = new Database("SiteSqlServer");
            _sprocsWithUserId = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);

            try
            {
                string q = "select specific_name from INFORMATION_SCHEMA.PARAMETERS WHERE PARAMETER_NAME = '@@UserID'";
                var spList = _database.Fetch<Dictionary<string, object>>(q);
                _sprocsWithUserId.UnionWith(spList.Select(_ => _.Single().Value.ToString()));
                _whitelist = ConfigurationManager.AppSettings["Whitelist"].ToString().Split(';');
            }
            catch
            {
                // todo
            }
        }

        [DnnAuthorize]
        [AllowAnonymous]
        [HttpPost()]
        // For the love of god let's change this to a JObject directly instead of parsing a text/plain before productionizing
        public HttpResponseMessage SP([FromBody] JObject arguments)
        {
            try
            {
                var args = arguments;
                CheckWhiteList(args);

                string spName = args["name"].ToString();
                // build exec and parameters   
                string query = $"exec {spName} ";
                var parameters = ((JObject)args["parameters"])?.ToObject<Dictionary<string, string>>(); // There's probably a better way but who cares
                if (parameters != null)
                {
                    query += string.Concat(parameters?.Where(_ => !string.Equals(_.Value, "-1", StringComparison.InvariantCultureIgnoreCase)).Select(_ => $" @@{_.Key} = '{_.Value.Trim()}', ")); // *always* named parameters
                }

                // if the sproc accepts userid, send it!
                if (_sprocsWithUserId.Contains(spName))
                {
                    query += $"@@UserId = {UserInfo.UserID}";
                }

                query = query.TrimEnd(new[] { ',', ' ' }); // If you think this is a hack remember: A microprocessor is silicon that we hacked to make it "work"

                List<Dictionary<string, object>> results;
                lock (_lock)
                {
                    results = _database.Fetch<Dictionary<string, object>>(query);
                }
                // This is because I've no idea how to default to application/json in DNN's weird webapi/services framework
                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(new { results = results }), System.Text.Encoding.UTF8, "application/json");
                return response;
            }
            catch (Exception ex)
            {
                //Log to DotNetNuke and reply with Error
                Exceptions.LogException(ex);
                var r = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                r.Content = new StringContent(JsonConvert.SerializeObject(ex), System.Text.Encoding.UTF8, "application/json");
                return r;
            }
        }

        private static void CheckWhiteList(JObject args)
        {
            // If the sp name is not in the whitelist, throw.
            try
            {
                if (!_whitelist.Any(_ => string.Equals(_, args["name"].ToString(), StringComparison.InvariantCultureIgnoreCase)))
                {
                    throw new SecurityException("Execution denied by whitelist");
                }
            }
            catch(Exception ex)
            {
                throw new Exception(@"Whitelist validation failed, check innerexception or add <add key=""Whitelist"" value=""dbo.tuvieja; dbo.dbo_GetUsers"" />", ex);
            }
        }
    }
}